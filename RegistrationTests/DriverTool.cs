﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Threading;
using System.Diagnostics;

namespace RegistrationTests
{
	internal class DriverTool
	{
		private RemoteWebDriver _driver;
		private DriverOptions _driverOptions;

		internal DriverTool(string urlToTest, string seleniumUrl, DriverOptions options)
		{
			_driverOptions = options;
			_driver = new RemoteWebDriver(new Uri(seleniumUrl), _driverOptions);
			_driver.Navigate().GoToUrl(urlToTest);
			//Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
			Trace.Listeners.Add(new ConsoleTraceListener());
			Trace.WriteLine("HELLO");
			Trace.WriteLine(_driver.SessionId.ToString());
		}


		internal void Quit()
		{
			_driver.Quit();
		}


		internal void RefreshPage()
		{
			Thread.Sleep(5000);
			_driver.Navigate().Refresh();
		}


		internal IWebElement GetFirstName()
		{
			return _driver.FindElementById("firstname");
		}


		internal IWebElement GetLastName()
		{
			return _driver.FindElementById("lastname");
		}


		internal IWebElement GetLogin()
		{
			return _driver.FindElementById("login");
		}


		internal void ChooseSuggestedLoginFromList()
		{
			Thread.Sleep(1000);
			_driver.FindElementByClassName("registration__pseudo-link").Click();
		}


		internal IWebElement GetPassword()
		{
			return _driver.FindElementById("password");
		}


		internal IWebElement GetPasswordConfirm()
		{
			return _driver.FindElementById("password_confirm");
		}

		internal IWebElement GetPhone()
		{
			return _driver.FindElementById("phone");
		}


		internal IWebElement GetEULACheckbox()
		{
			return _driver.FindElementByName("eula_accepted");
		}


		internal IWebElement GetRegisterButton()
		{
			return _driver.FindElementByClassName("Button2_type_submit");
		}


		internal IWebElement GetLoginSuggetstions()
		{
			return _driver.FindElementByClassName("form__login-suggest");
		}

		internal bool IsFirstnameInvalid()
		{
			return Convert.ToBoolean(GetFirstName().GetAttribute("aria-invalid"));
		}


		internal bool IsLastnameInvalid()
		{
			return Convert.ToBoolean(GetLastName().GetAttribute("aria-invalid"));
		}


		internal bool IsLoginInvalid()
		{
			return Convert.ToBoolean(GetLogin().GetAttribute("aria-invalid"));
		}


		internal bool IsPasswordInvalid()
		{
			return Convert.ToBoolean(GetPassword().GetAttribute("aria-invalid"));
		}


		internal bool IsPasswordConfirmInvalid()
		{
			return Convert.ToBoolean(GetPasswordConfirm().GetAttribute("aria-invalid"));
		}


		internal bool IsPhoneInvalid()
		{
			return Convert.ToBoolean(GetPhone().GetAttribute("aria-invalid"));
		}


		internal bool IsEULACheckboxChecked()
		{
			return Convert.ToBoolean(GetEULACheckbox().GetAttribute("aria-checked"));
		}


		internal bool IsRegisterButtonDisabled()
		{
			return Convert.ToBoolean(GetRegisterButton().GetAttribute("aria-disabled"));
		}

	}
}
