using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace RegistrationTests
{
	[TestClass]
	public class TestsChrome
	{

		static internal DriverTool registrationPage;


		[ClassInitialize]
		public static void SetupChrome(TestContext context)
		{
			var options = new ChromeOptions();
			options.AddAdditionalCapability("enableVNC", true, true);
			options.AddAdditionalCapability("enableVideo", true, true);
			options.AddAdditionalCapability("enableLog", true, true);
			registrationPage = new DriverTool("https://passport.yandex.ru/registration",
				"http://84.38.181.51:4444/wd/hub",
				options);
		}


		[ClassCleanup]
		public static void DisposeChrome()
		{
			registrationPage.Quit();
		}


		[TestMethod]
		public void ValidDataPassesValidation()
		{
			//Arrange
			registrationPage.RefreshPage();

			//Act - Positive test case
			registrationPage.GetFirstName().SendKeys("Konstantin");
			registrationPage.GetLastName().SendKeys("Smorodin");
			registrationPage.GetLogin().SendKeys("Kostya4623411");
			registrationPage.GetPassword().SendKeys("qwerty123qwerty");
			registrationPage.GetPasswordConfirm().SendKeys("qwerty123qwerty");
			//registrationPage.GetPhone().SendKeys("9999999999");
			registrationPage.GetRegisterButton().Click();

			//Assert
			Assert.IsFalse(registrationPage.IsFirstnameInvalid());
			Assert.IsFalse(registrationPage.IsLastnameInvalid());
			Assert.IsFalse(registrationPage.IsLoginInvalid());
			Assert.IsFalse(registrationPage.IsPasswordInvalid());
			Assert.IsFalse(registrationPage.IsPasswordConfirmInvalid());
			//Assert.IsFalse(registrationPage.IsPhoneInvalid());
		}


		[TestMethod]
		public void InvalidDataNotPassesValidation()
		{
			//Arrange
			registrationPage.RefreshPage();

			//Act - Negative test case
			registrationPage.GetFirstName().SendKeys(Keys.Enter);
			registrationPage.GetLastName().SendKeys(Keys.Enter);
			registrationPage.GetLogin().SendKeys(Keys.Enter);
			registrationPage.GetPassword().SendKeys(Keys.Enter);
			registrationPage.GetPasswordConfirm().SendKeys(Keys.Enter);
			registrationPage.GetPhone().SendKeys(Keys.Enter);
			registrationPage.GetRegisterButton().Click();

			//Assert
			Assert.IsTrue(registrationPage.IsFirstnameInvalid());
			Assert.IsTrue(registrationPage.IsLastnameInvalid());
			Assert.IsTrue(registrationPage.IsLoginInvalid());
			Assert.IsTrue(registrationPage.IsPasswordInvalid());
			Assert.IsTrue(registrationPage.IsPasswordConfirmInvalid());
			Assert.IsTrue(registrationPage.IsPhoneInvalid());
		}


		[TestMethod]
		public void IfEulaIsUncheckedRegisterIsDisabled()
		{
			//Arrange
			registrationPage.RefreshPage();

			//Act
			if (registrationPage.IsEULACheckboxChecked())
			{
				registrationPage.GetFirstName().SendKeys("IfEulaIsUncheckedRegisterIsDisabled");
				registrationPage.GetEULACheckbox().Click();
			}

			//Assert
			Assert.IsTrue(registrationPage.IsRegisterButtonDisabled());
		}


		[TestMethod]
		public void IfEulaIsCheckedRegisterIsEnabled()
		{
			//Arrange
			registrationPage.RefreshPage();

			//Act
			if (!registrationPage.IsEULACheckboxChecked())
			{
				registrationPage.GetFirstName().SendKeys("IfEulaIsCheckedRegisterIsEnabled");
				registrationPage.GetEULACheckbox().Click();
			}

			//Assert
			Assert.IsFalse(registrationPage.IsRegisterButtonDisabled());
		}

		[TestMethod]
		public void UsedLoginIsInvalid()
		{
			//Arrange
			registrationPage.RefreshPage();

			//Act
			registrationPage.GetLogin().SendKeys("tolik.trollik"); //this nickname already exists in yandex.ru
			registrationPage.GetRegisterButton().Click();

			//Assert
			Assert.IsTrue(registrationPage.IsLoginInvalid());
		}


		[TestMethod]
		public void FormSuggestsValidLogins()
		{
			//Arrange
			registrationPage.RefreshPage();
			registrationPage.GetFirstName().SendKeys("Konstantin");
			registrationPage.GetLastName().SendKeys("Smorodin");
			registrationPage.GetLogin().SendKeys("tolik.trollik"); //this nickname already exists in yandex.ru

			//Act
			registrationPage.ChooseSuggestedLoginFromList();

			//Assert
			Assert.IsFalse(registrationPage.IsLoginInvalid());
		}

	}
}
